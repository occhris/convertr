<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

/*
Usage example:

php testr.php file/AddAfter
*/

namespace Convertr;

error_reporting(E_ALL);
ini_set('display_errors', '1');

require(dirname(__FILE__) . '/convertr.php');

$testr = new Testr();

$test = null;

if (isset($_GET['test'])) {
    $test = $_GET['test'];
}

if (isset($_SERVER['argv'][1])) {
    $test = $_SERVER['argv'][1];
}

if (!is_null($test)) {
    if (substr($test, -5) == '.test') {
        $test = substr($test, 0, strlen($test) - 5);
    }

    $testr->run_test($test);
} else {
    $testr->run_all_tests();
}
