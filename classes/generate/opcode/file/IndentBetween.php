<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\generate\opcode\file;

class IndentBetween extends \Convertr\generate\opcode\Opcode
{
    public function get_precedence()
    {
        return parent::PRECEDENCE_HIGHEST - 1;
    }

    public function generate_opcode($old, $new, $all_files, $extension)
    {
        // TODO
        return null;
    }
}
