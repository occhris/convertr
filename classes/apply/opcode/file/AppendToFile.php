<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\apply\opcode\file;

class AppendToFile extends \Convertr\apply\opcode\Opcode
{
    public function apply_opcode($filesystem, $opcode_parameters)
    {
        list($file, $text) = $opcode_parameters;

        $contents = $filesystem->read($file);
        $contents .= $text;
        $filesystem->write($file, $contents);

        return true;
    }
}
