<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr;

class FileSystemDisk extends FileSystem
{
    private $path, $extension;

    private $cached_crc32 = null, $cached_files, $cached_files_and_data;

    public function __construct($path, $extension)
    {
        $this->init();

        if (substr($path, -1) != '/' && substr($path, -1) != '\\') {
            $path .= '/';
        }
        $this->path = $path;
        $this->extension = $extension;
    }

    public function get_all_files()
    {
        if (!is_null($this->cached_files)) {
            return $this->cached_files;
        }

        $files = array();
        $dh = opendir($this->path);
        while (($f = readdir($dh)) !== false) {
            if (substr($f, -strlen($this->extension) - 1) == '.' . $this->extension) {
                $files[] = $f;
            }
        }
        closedir($dh);

        $this->cached_files = $files;

        return $ret;
    }

    public function get_all_files_and_data()
    {
        if (!is_null($this->cached_files_and_data)) {
            return $this->cached_files_and_data;
        }

        $files = $this->get_all_files();

        $files_and_data = array();
        foreach ($files as $file) {
            $files_and_data[$file] = $this->read($file);
        }

        $this->cached_files_and_data = $files_and_data;

        return $files_and_data;
    }

    public function read($file)
    {
        if (!is_file($this->path . $file)) {
            return null;
        }

        return file_get_contents($this->path . $file);
    }

    public function write($file, $data)
    {
        file_put_contents($this->path . $file, $data);

        $this->clear_caching();
    }

    public function delete($file)
    {
        unlink($this->path . $file);

        $this->clear_caching();
    }

    public function rename($from, $to)
    {
        if (is_file($this->path . $to)) {
            unlink($this->path . $from);
        } else {
            rename($this->path . $from, $this->path . $to);
        }

        $this->clear_caching();
    }

    public function get_crc32()
    {
        if (!is_null($this->cached_crc32)) {
            return $this->cached_crc32;
        }

        $crc32 = array();
        foreach ($this->get_all_files() as $file) {
            $crc32[$file] = hexdec(hash_file('crc32b', $this->path . $file));
        }
        $this->cached_crc32 = $crc32;

        return $crc32;
    }

    private function clear_caching()
    {
        super::clear_caching();

        $this->cached_crc32 = null;
        $this->cached_files = null;
        $this->cached_files_and_data = null;
    }
}
