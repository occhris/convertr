<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr;

function _convertr_autoload($class)
{
    if (strpos($class, '\\') !== false) {
        if (substr($class, 0, 9) != 'Convertr\\') {
            if ($_SERVER['SCRIPT_NAME'] == 'testr.php') {
                throw new \Exception('Non-recognised class ' . $class);
            }

            return;
        } else {
            $class = substr($class, 9);
        }
    }

    $path = dirname(__FILE__) . '/classes/' . str_replace('\\', '/', $class) . '.php';
    if (is_file($path)) {
        include($path);
    } else {
        if ($_SERVER['SCRIPT_NAME'] == 'testr.php') {
            throw new \Exception('Cannot find ' . $class);
        }
    }
}

function convertr_generate($path_old, $path_new, $extension)
{
    $filesystem_old = new FileSystemDisk($path_old);
    $filesystem_new = new FileSystemDisk($path_new);
    $engine = new generate\Generate($filesystem_old, $filesystem_new);

    return $engine->generate($extension);
}

function convertr_apply($path, $operations, $extension, $simulate)
{
    $filesystem = new FileSystemDisk($path);

    if ($simulate) {
        // Copy to in-memory filesystem
        $filesystem = new \Convertr\FileSystemTestr($filesystem->get_all_files_and_data(), $extension);
    }

    $engine = new apply\Apply($filesystem);
    $engine->apply($operations, $extension);
}

spl_autoload_register('Convertr\_convertr_autoload');
