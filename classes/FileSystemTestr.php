<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr;

class FileSystemTestr extends FileSystem
{
    private $files, $extension;

    public function __construct($files, $extension)
    {
        $this->init();

        $this->files = $files;
        $this->extension = $extension;
    }

    public function get_all_files()
    {
        $files = array();
        foreach (array_keys($this->files) as $file) {
            if (substr($file, -strlen($this->extension) - 1) == '.' . $this->extension) {
                $files[] = $file;
            }
        }

        return $files;
    }

    public function get_all_files_and_data()
    {
        $files = $this->get_all_files();

        $files_and_data = array();
        foreach ($files as $file) {
            $files_and_data[$file] = $this->read($file);
        }

        return $files_and_data;
    }

    public function read($file)
    {
        if (!isset($this->files[$file])) {
            return null;
        }

        return $this->files[$file];
    }

    public function write($file, $data)
    {
        $this->files[$file] = $data;

        $this->clear_caching();
    }

    public function delete($file)
    {
        unset($this->files[$file]);

        $this->clear_caching();
    }

    public function rename($from, $to)
    {
        $this->files[$to] = $this->files[$from];
        unset($this->files[$from]);

        $this->clear_caching();
    }
}
