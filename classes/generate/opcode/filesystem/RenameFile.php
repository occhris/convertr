<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\generate\opcode\filesystem;

class RenameFile extends \Convertr\generate\opcode\Opcode
{
    public function get_precedence()
    {
        return parent::PRECEDENCE_HIGHEST;
    }

    public function generate_opcode($old, $new, $all_files, $extension)
    {
        $_old_files = $old->get_all_files();
        $_new_files = $new->get_all_files();

        if ($_old_files == $_new_files) {
            return null;
        }

        $old_files = array_flip($_old_files);
        $new_files = array_flip($_new_files);

        $has_only_in_old = false;
        foreach (array_keys($old_files) as $file) {
            if (!isset($new_files[$file])) {
                $has_only_in_old = true;
            }
        }
        if (!$has_only_in_old) {
            return null;
        }

        $only_in_new = array();
        foreach (array_keys($new_files) as $file) {
            if (!isset($old_files[$file])) {
                $only_in_new[$file] = $this->frequency_analysis($new->read($file));
            }
        }

        if (count($only_in_new) == 0) {
            return null;
        }

        foreach (array_keys($old_files) as $file) {
            if (!isset($new_files[$file])) {
                $old_freqs = $this->frequency_analysis($old->read($file));

                $best_distance = null;
                foreach ($only_in_new as $_newfile => $new_freqs) {
                    $distance = $this->frequency_difference($old_freqs, $new_freqs);

                    if (($best_distance === null) || ($distance < $best_distance)) {
                        $newfile = $_newfile;
                    }
                }

                return array(
                    'RenameFile',
                    $file,
                    $newfile,
                );
            }
        }

        return null;
    }

    // We use frequency analysis in ASCII space to get a feel for how similar files are. It's rough, but we need to be reasonably performant

    function frequency_difference($a, $b)
    {
        $diff = 0;

        for ($i = 0; $i < 255; $i++) {
            $cnt_a = isset($a[$i]) ? $a[$i] : 0;
            $cnt_b = isset($b[$i]) ? $b[$i] : 0;

            $diff += abs($cnt_a - $cnt_b);
        }

        return $diff;
    }

    private function frequency_analysis($data)
    {
        $chars = array();
        $len = strlen($data);
        for ($i = 0; $i < $len; $i++) {
            $ascii = ord($data[$i]);
            if (!isset($chars[$ascii])) {
                $chars[$ascii] = 0;
            }
            $chars[$ascii]++;
        }
        return $chars;
    }
}
