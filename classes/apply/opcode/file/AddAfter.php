<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\apply\opcode\file;

class AddAfter extends \Convertr\apply\opcode\Opcode
{
    public function apply_opcode($filesystem, $opcode_parameters)
    {
        list($file, $precontext, $text) = $opcode_parameters;

        $contents = $filesystem->read($file);

        if (strpos($contents, $precontext) === false) {
            return false;
        }

        $contents = str_replace($precontext, $precontext . $text, $contents);

        $filesystem->write($file, $contents);

        return true;
    }
}
