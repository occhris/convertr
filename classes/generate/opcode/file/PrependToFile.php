<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\generate\opcode\file;

class PrependToFile extends \Convertr\generate\opcode\Opcode
{
    public function get_precedence()
    {
        return parent::PRECEDENCE_HIGHEST;
    }

    public function generate_opcode_from_diff($diff)
    {
        $first_element = $diff[0];
        if (is_a($first_element, 'FineDiffInsertOp')) {
            return array(
                $first_element->text
            );
        }

        return null;
    }
}
