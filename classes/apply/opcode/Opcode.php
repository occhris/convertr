<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\apply\opcode;

abstract class Opcode
{
    abstract public function apply_opcode($filesystem, $opcode_parameters);
}
