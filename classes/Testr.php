<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr;

class Testr
{
    private $test_path;

    public function __construct()
    {
        $this->test_path = dirname(dirname(__FILE__)) . '/tests/';
    }

    public function run_all_tests()
    {
        $dh = opendir($this->test_path);
        while (($f = readdir($dh)) !== false) {
            if (substr($f, -5) == '.test') {
                $this->run_test($f);
            }
        }
        closedir($dh);
    }

    public function run_test($test_name)
    {
        list($files_old, $files_new, $expects_output, $comments) = $this->read_test($test_name);
        $actual_output = $this->execute_test($files_old, $files_new);
        if ($expects_output !== $actual_output) {
            throw new \Exception('Test failed: ' . $test_name . ', got: ' . "\n\n" . var_export($actual_output, true) . "\n\n" . 'expected: ' . "\n\n" . var_export($expects_output, true));
        }

        echo 'PASSED: '.$test_name."\n";
    }

    private function read_test($test_name)
    {
        $contents = file_get_contents($this->test_path . $test_name . '.test');

        $parts = preg_split("#(^|\n)-(?=\n|$)#", $contents);
        if (count($parts) < 3) {
            throw new \Exception('Test ' . $test_name . ' does not have enough parts to it');
        }

        // We didn't bind the leading line break on each portion (because of "\n-\n-\n" potential leading to overlapping delimiters) -- so strip it now
        foreach ($parts as $i => &$part) {
            if ($i != 0) {
                if (substr($part, 0, 1) == "\n") {
                    $part = substr($part, 1);
                }
            }
        }

        $old = $this->parse_string_filesystem($parts[0]);
        $new = $this->parse_string_filesystem($parts[1]);
        $expects = eval('return ' . $parts[2] . ';');
        $comments = isset($parts[3]) ? $parts[3] : '';

        return array($old, $new, $expects, $comments);
    }

    private function parse_string_filesystem($data)
    {
        $regexp = '#(^\[.*\]$\n)#m';
        $parts = preg_split($regexp, $data, null, PREG_SPLIT_DELIM_CAPTURE);

        $files = array();
        $current_file = null;
        $current_file_data = '';
        foreach ($parts as $part) {
            if (preg_match($regexp, $part) != 0) {
                if (!is_null($current_file)) {
                    $files[$current_file] = $current_file_data;
                }
                $current_file = trim($part, "[]\n");
                $current_file_data = '';
            } else {
                $current_file_data .= $part;
            }
        }
        if (!is_null($current_file)) {
            $files[$current_file] = $current_file_data;
        }

        return $files;
    }

    private function execute_test($files_old, $files_new)
    {
        $filesystem_old = new FileSystemTestr($files_old, 'txt');
        $filesystem_new = new FileSystemTestr($files_new, 'txt');
        $engine = new generate\Generate($filesystem_old, $filesystem_new);

        return $engine->generate('txt');
    }
}
