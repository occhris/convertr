<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr;

abstract class OpcodeManager
{
    protected function find_all_opcodes($under)
    {
        return array_merge(
            $this->find_all_opcodes_type($under . '/opcode/filesystem'),
            $this->find_all_opcodes_type($under . '/opcode/file')
        );
    }

    private function find_all_opcodes_type($type)
    {
        $opcodes = array();
        $full_dir = dirname(__FILE__) . '/' . $type;
        if (!is_dir($full_dir)) throw new \Exception('Cannot find '.$full_dir);
        $dh = opendir($full_dir);
        while (($f = readdir($dh)) !== false) {
            if (substr($f, -4) == '.php') {
                $opcode_class = '\\Convertr\\' . str_replace('/', '\\', $type . '/' . basename($f, '.php'));
                $object = new $opcode_class();

                if (method_exists($object, 'get_precedence')) {
                    $sort_key = str_pad($object->get_precedence(), 10, '0', STR_PAD_LEFT) . '_' . $opcode_class;
                } else {
                    $sort_key = preg_replace('#^.*\\\\([^\\\\]*)$#', '$1', $opcode_class);
                }

                $opcodes[$sort_key] = $object;
            }
        }
        closedir($dh);
        krsort($opcodes);

        $opcodes_keyed = array();
        foreach ($opcodes as $object) {
            $opcode_key = preg_replace('#^.*\\\\([^\\\\]*)$#', '$1', get_class($object));
            $opcodes_keyed[$opcode_key] = $object;
        }

        return $opcodes_keyed;
    }
}
