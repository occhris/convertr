<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\generate\opcode\file;

class AppendToFile extends \Convertr\generate\opcode\Opcode
{
    public function get_precedence()
    {
        return parent::PRECEDENCE_HIGHEST;
    }

    public function generate_opcode_from_diff($diff)
    {
        $last_element = $diff[count($diff) - 1];
        if (is_a($last_element, 'FineDiffInsertOp')) {
            return array(
                $last_element->text
            );
        }

        return null;
    }
}
