<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\generate\opcode\file;

class AddBefore extends \Convertr\generate\opcode\Opcode
{
    public function get_precedence()
    {
        return parent::PRECEDENCE_HIGHEST - 1;
    }

    public function generate_opcode_from_diff($diff, $filename, $old)
    {
        $last_i = count($diff) - 1;
        foreach ($diff as $i => $element) {
            if ($i != $last_i) {
                if (is_a($element, 'FineDiffInsertOp')) {
                    $post_element = $diff[$i + 1];

                    if (is_a($post_element, 'FineDiffCopyOp')) {
                        $minimum_context = $this->get_minimum_postcontext($filename, $old, $post_element->text, $element->text);
                        if ($minimum_context !== null) {
                            return array(
                                $minimum_context,
                                $element->text
                            );
                        }
                    }
                }
            }
        }

        return null;
    }
}
