<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\generate\opcode;

abstract class Opcode
{
    const PRECEDENCE_HIGHEST = 100;
    const PRECEDENCE_MODERATE = 50;
    const PRECEDENCE_LOWEST = 1;

    public abstract function get_precedence();

    public function generate_opcode($old, $new, $all_files, $extension)
    {
        if (method_exists($this, 'generate_opcode_from_diff')) {
            foreach ($all_files as $filename) {
                $diff = $new->generate_diff_from_older($old, $filename);

                if ((is_array($diff)) && ($diff !== array())) {
                    $test = $this->generate_opcode_from_diff($diff, $filename, $old);
                    if ($test !== null) {
                        array_splice($test, 0, 0, array(preg_replace('#^.*\\\\#', '', get_class($this)), $filename)); // Add class name and filename at positions #0-#1
                        return $test;
                    }
                }
            }
        }

        return null;
    }

    protected function get_minimum_precontext($filename, $fs, $precontext, $text)
    {
        $contents = $fs->read($filename);
        if (substr_count($contents, $precontext) == 1) {
            $minimum_context = '';
            $i = strlen($precontext) - 1;
            do {
                $minimum_context .= $precontext[$i];
                $invalid = (substr_count($contents, $minimum_context) > 1);
                $i--;
            }
            while ($invalid);
            return $minimum_context;
        }

        return null; // Could not get unique match
    }

    protected function get_minimum_postcontext($filename, $fs, $postcontext, $text)
    {
        $contents = $fs->read($filename);
        if (substr_count($contents, $postcontext) == 1) {
            $minimum_context = '';
            $i = 0;
            do {
                $minimum_context = $postcontext[$i] . $minimum_context;
                $invalid = (substr_count($contents, $minimum_context) > 1);
                $i++;
            }
            while ($invalid);
            return $minimum_context;
        }

        return null; // Could not get unique match
    }
}
