<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\apply;

class Apply extends \Convertr\OpcodeManager
{
    private $filesystem;

    public function __construct($filesystem)
    {
        $this->filesystem = $filesystem;
    }

    function apply($operations, $extension)
    {
        $opcode_objects = $this->find_all_opcodes('apply');

        $errors = array();

        foreach ($operations as $operation) {
            $operation_full = $operation;

            $opcode = array_shift($operation);
            $opcode_parameters = $operation;
            if (!isset($opcode_objects[$opcode])) {
                throw new Exception('Could not find opcode' . $opcode);
            }
            $test = $opcode_objects[$opcode]->apply_opcode($this->filesystem, $opcode_parameters);
            if (!$test) {
                $errors[] = $operation_full;
            }
        }

        return $errors;
    }
}
