<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\generate\opcode\filesystem;

class AddFile extends \Convertr\generate\opcode\Opcode
{
    public function get_precedence()
    {
        return parent::PRECEDENCE_HIGHEST - 1;
    }

    public function generate_opcode($old, $new, $all_files, $extension)
    {
        $old_files = array_flip($old->get_all_files());
        $new_files = $new->get_all_files();

        foreach ($new_files as $file) {
            if (!isset($old_files[$file])) {
                return array(
                    'AddFile',
                    $file,
                    $new->read($file),
                );
            }
        }

        return null;
    }
}
