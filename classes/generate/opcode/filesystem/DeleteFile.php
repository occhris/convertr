<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\generate\opcode\filesystem;

class DeleteFile extends \Convertr\generate\opcode\Opcode
{
    public function get_precedence()
    {
        return parent::PRECEDENCE_HIGHEST - 1;
    }

    public function generate_opcode($old, $new, $all_files, $extension)
    {
        $old_files = $old->get_all_files();
        $new_files = array_flip($new->get_all_files());

        foreach ($old_files as $file) {
            if (!isset($new_files[$file])) {
                return array(
                    'DeleteFile',
                    $file,
                );
            }
        }

        return null;
    }
}
