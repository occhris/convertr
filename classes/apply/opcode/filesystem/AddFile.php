<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\apply\opcode\filesystem;

class AddFile extends \Convertr\apply\opcode\Opcode
{
    public function apply_opcode($filesystem, $opcode_parameters)
    {
        list($filepath, $data) = $opcode_parameters;

        if ($filesystem->read($filepath) !== null) {
            // Already exists
            return false;
        }

        $filesystem->write($filepath, $data);
        return true;
    }
}
