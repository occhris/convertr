<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\generate;

class Generate extends \Convertr\OpcodeManager
{
    private $filesystem_old, $filesystem_new;

    public function __construct($filesystem_old, $filesystem_new)
    {
        $this->filesystem_old = $filesystem_old;
        $this->filesystem_new = $filesystem_new;
    }

    public function generate($extension)
    {
        $filesystem_temp = new \Convertr\FileSystemTestr($this->filesystem_old->get_all_files_and_data(), $extension);

        $apply_ob = new \Convertr\apply\Apply($filesystem_temp);

        $all_files = $this->all_files($filesystem_temp, $this->filesystem_new);

        $opcodes = array(); // Ordered list of opcodes to convert $filesystem_temp to $this->filesystem_new

        $opcode_objects = $this->find_all_opcodes('generate');
        foreach ($opcode_objects as $opcode_object) {
            do {
                $test = $opcode_object->generate_opcode($filesystem_temp, $this->filesystem_new, $all_files, $extension);
                if (!is_null($test)) {
                    $opcodes[] = $test;

                    $all_files = $this->all_files($filesystem_temp, $this->filesystem_new);

                    $old_crc32 = $filesystem_temp->get_crc32($extension);

                    $apply_test = $apply_ob->apply(array($test), $extension); // We need it so that next opcode search has updated state
                    if ($apply_test !== array()) {
                        throw new \Exception('Error applying generated opcode:' . "\n" . var_export($test, true));
                    }

                    if ($filesystem_temp->get_crc32($extension) == $old_crc32) {
                        throw new \Exception('Generated operation that does nothing');
                    }

                    if ($filesystem_temp->get_crc32($extension) == $this->filesystem_new->get_crc32($extension)) {
                        break 2;
                    }
                }
            } while (!is_null($test));
        }

        if ($filesystem_temp->get_crc32($extension) != $this->filesystem_new->get_crc32($extension)) {
            throw new \Exception('Internal error - not everything covered in diff');
        }

        return $opcodes;
    }

    private function all_files($old, $new)
    {
        return array_unique(array_merge($old->get_all_files(), $new->get_all_files()));
    }
}
