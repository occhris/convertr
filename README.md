(This is a work-in-progress project, not ready for usage at this time)

Convertr is an advanced pluggable diff engine, built on top of a traditional diff engine (finediff).

It supports operands that are far more flexible than a regular diff engine so that diffs can be applied on top of heavily-edited files.

Diffs can be highly fragile - they break very easily for small changes of context, and are not end-user friendly. Convertr operations lock on narrower (valid/unique) contexts.

A context is simply the shortest unique character sequence we can find that precedes/proceeds where we need to perform our operation.

It supports recursive operands also (for example, move some text to a different part of a file and then indent it). This minimises the size of our diffs, while maximising their robustness.

Convertr works on a directory level, for a particular set of file extensions in that directory.

Convertr was originally developed to allow for automatic upgrading of user themes across versions of Composr CMS. Diff signatures are distributed between versions and automatic applied to user themes by the upgrader.

Requirements
------------

Requires PHP 5.3+.

Usage
-----

Just include the convertr.php file and call convertr_generate or convertr_apply with the necessary parameters.

Code structure
--------------

The code is heavily unit tested, heavily OOP, and uses PHP namespaces. This leads to its high level of modularity.

Each opcode is implemented as a generate class and an apply class.

First a traditional diff runs, then each opcode is queried to see if it can represent traditional diff line(s) in our better way. Priorities are used so that particular opcodes can take precedence in a sensible way (e.g. indenting is higher precedence than a string replace).

Reporting bugs / requesting features
------------------------------------

See the ocProducts tracker:
http://compo.sr/tracker/set_project.php?project_id=12

License
-------

Copyright (c) 2015 ocProducts Ltd

Licensed under The MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
