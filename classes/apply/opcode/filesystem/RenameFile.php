<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\apply\opcode\filesystem;

class RenameFile extends \Convertr\apply\opcode\Opcode
{
    public function apply_opcode($filesystem, $opcode_parameters)
    {
        list($filepath, $newfilepath) = $opcode_parameters;

        if ($filesystem->read($filepath) === null) {
            // Source does not exist
            return false;
        }

        if ($filesystem->read($newfilepath) !== null) {
            // Target already exists
            return false;
        }

        $filesystem->rename($filepath, $newfilepath);
        return true;
    }
}
