<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr\apply\opcode\file;

class AddBefore extends \Convertr\apply\opcode\Opcode
{
    public function apply_opcode($filesystem, $opcode_parameters)
    {
        list($file, $postcontext, $text) = $opcode_parameters;

        $contents = $filesystem->read($file);

        if (strpos($contents, $postcontext) === false) {
            return false;
        }

        $contents = str_replace($postcontext, $text . $postcontext, $contents);

        $filesystem->write($file, $contents);

        return true;
    }
}
