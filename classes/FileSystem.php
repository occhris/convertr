<?php /*

 Convertr
 Copyright (c) ocProducts, 2015-2015

*/

/**
 * @license    MIT Licence
 * @copyright  ocProducts Ltd
 * @package    Convertr
 */

namespace Convertr;

abstract class FileSystem
{
    const DIFF_ONLY_IN_OLDER = 1;
    const DIFF_ONLY_IN_NEWER = 2;

    protected $id;
    private $cached_diff_from;

    abstract public function get_all_files();

    abstract public function get_all_files_and_data();

    abstract public function read($file);

    abstract public function write($file, $data);

    abstract public function delete($file);

    abstract public function rename($from, $to);

    protected function init()
    {
        $this->id = uniqid('');
        $this->diff_from = array();
    }

    public function get_crc32()
    {
        $crc32 = array();
        foreach ($this->get_all_files() as $file) {
            $crc32[$file] = crc32($this->read($file));
        }

        return $crc32;
    }

    public function generate_diff_from_older($older_filesystem, $filename)
    {
        if (isset($this->cached_diff_from[$older_filesystem->id][$filename])) {
            return $this->cached_diff_from[$older_filesystem->id][$filename];
        }

        $old = $older_filesystem->read($filename);
        if (is_null($old)) {
            return self::DIFF_ONLY_IN_NEWER;
        }
        $new = $this->read($filename);
        if (is_null($new)) {
            return self::DIFF_ONLY_IN_OLDER;
        }

        require_once(dirname(dirname(__FILE__)) . '/third_party/finediff.php');
        $diff = new \FineDiff($old, $new, \FineDiff::$characterGranularity);
        $this->cached_diff_from[$older_filesystem->id][$filename] = $diff->edits;

        return $diff->edits;
    }

    protected function clear_caching()
    {
        $this->cached_diff_from = array();
    }
}
